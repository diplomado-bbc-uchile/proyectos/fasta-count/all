# 1. Crear Resource Group (RG)

Un resource group es un objecto que organiza servicios en Azure. Lo podemos crear con este link:
https://portal.azure.com/#create/Microsoft.ResourceGroup

# 2. Migración Database**

## 2.1 Crear MySQL Server en Azure

Crear base de datos en Azure con el link:
https://portal.azure.com/#create/Microsoft.MySQLServer

Ocupar la opción `Flexible server`, en las pestañas:

Basics:
- Server Name: fsc
- Region: East US
- MySQL Version: 5.7
- Workload type: For development or hobby projects
- Compute + storage: Burstable, B1s
- Admin username: fasta
- Password: p@ssw0rd

Networking:
- Firewall Rules
  - Allow public access from any Azure service within Azure to this server
  - Add current client IP address

## 2.2 Conectar a la base de datos

Conexión:
```
mysql -h fsc.mysql.database.azure.com -u fasta -p
```

Revisar que este vacía.
```
show databases;
```

Crear database fsc
```
CREATE DATABASE fsc;
```

## 2.3 Crear dump de la base de datos local

Obtener la IP interna del container:
```
DATABASE_IP=$(docker inspect fsc-database-1 | jq -r .[].NetworkSettings.Networks.fsc_db.IPAddress)
```

Generar dump:
```
mysqldump -h $DATABASE_IP --no-tablespaces -u fasta -p fsc > fsc_dump.sql
```

## 2.4 Importar dump en la nube

```
mysql -h fsc.mysql.database.azure.com -u fasta -p fsc < fsc_dump.sql
```

Revisar que la base de datos se importó exitosamente:

- Conectar a la BBDD:
```
mysql -h fsc.mysql.database.azure.com -u fasta -p fsc
```

- Revisar valores:
```
MySQL [fsc]> show tables;
MySQL [fsc]> select * from uploads; 
```

## 2.5 Actualizar docker-compose

Ahora que tenemos nuestra base de datos lista en la nube, debemos cambiar nuestro docker-compose para que la api apunte a esta nueva BBDD.

Add env variable
```
MYSQL_SSL_CA_PATH=/app/DigiCertGlobalRootCA.crt.pem
```

Vamos a observar que el delay de la comunicación.

# 3. Migración API

## 3.1 Crear el servicio Storage Account

https://portal.azure.com/#create/Microsoft.StorageAccount

- Basics:
  - Resource group: FSC
  - Storage account name: storagefsc
  - Region: EastUS2
  - Performance: 
  - Redundancy: Local
- Review + create

Una vez el Storage Account creado, crear un container llamado `uploads` con el Public Access Level "Blob"

En Access keys copiar el Connection String y hacer los cambios en el container instance:

## 3.2 Crear servicio Container Instance

https://portal.azure.com/#create/Microsoft.ContainerInstances

- Basics:
  - Image source: Other registry
  - Image type: Public
  - Image: `<docker-username>/fsc-api:storage-out`
  - Size: 1 vcpu, 1GiB
- Networking
  - DNS name label: apifsc 
  - Ports: 443
- Advanced
  - Environment variables
    - FLASK_APP=api.py
    - FLASK_RUN_PORT=443
    - MYSQL_HOST=fsc.mysql.database.azure.com
    - MYSQL_DB=fsc
    - MYSQL_USER=fsc
    - MYSQL_PASSWORD=55equenceCount
    - MYSQL_SSL_CA_PATH=/app/DigiCertGlobalRootCA.crt.pem
    - AZURE_STORAGE_CONNECTION_STRING=<security-connection-string> *
  - Command override: `[ "flask", "run", "--host=0.0.0.0", "--cert=adhoc" ]`
- Review + create

*El valor debe ser tomado del paso 3.1.

> Para probar con el frontend local, hay que cambiar en la carpeta web hacia la rama `storage-out`:
> - Ejecutar el comando `cd web && git checkout storage-out && git pull && cd ..`
> - Y en el docker-compose agregar la variable de entorno **REACT_APP_BASE_DOWNLOAD_URL** usando el valor `https://<storage-account-name>.blob.core.windows.net/uploads`

Al acceder localmente el front "http://localhost:3000"

# 4. Migración Frontend

## 4.1 Compilación

Definir variables de entorno para la compilación:
```
export REACT_APP_API_URL=http://<azure-container-instance-url>
export REACT_APP_BASE_DOWNLOAD_URL=https://<storage-account-name>.blob.core.windows.net/uploads
```

Instalar dependencias y ejecutar build:
```
yarn install && yarn build
```

Una vez finalizada la compilación, vamos a tener una carpeta llamada `build` con los archivos que tenemos que subir en el servicio correspondiente.

## 4.2 Upload del servicio

En el Storage Account que se creo en el paso 3.1, activar el `Static website` y hacer el upload de los archivos construidos en el paso 4.1 (archivos de la carpeta build) en el container `$web`.

> Definir en el campo `Index document name` el valor **index.html**

La URL de acceso puede ser encontrada en el campo **Primary endpoint** en la pestaña Static website.