# All

Este repositorio tiene el objetivo de facilitar el despliegue completo de la aplicación Fasta Sequence Count (FSC) a través de la herramienta **docker-compose**.

## Requisitos 

- [Docker >= 20.10](https://docs.docker.com/get-docker/)
- [Docker-compose >= 1.29](https://docs.docker.com/compose/install/)

## Run

- Clonar los repositorios

```shell
git clone --recurse-submodules https://gitlab.com/diplomado-bbc-uchile/proyectos/fasta-count/all.git fsc
```

- Ejecutar la aplicación

```shell
cd fsc && docker compose up --build
```

## Stop

Ejecutar un `Ctrl + c` para parar la ejecución.

## Clean

Para limpar las imágenes generadas y el contenido del volumen de la BBDD, ejecutar:

```shell
docker compose down -v
```

# Migrando cada componente a la nube

- [Procedimiento](https://gitlab.com/diplomado-bbc-uchile/projects/fsc/all/-/blob/master/procedimiento.md)